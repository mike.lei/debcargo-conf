General
=======

coreutils is packaged in an unusual way. The binaries are published on
crates.io. As we don't want to package them separately, we have to do
some voodoo magic to make it more maintainable but not following
the typical debcargo process.

To be coinstallable with GNU coreutils, manpages are called rust-$PROGRAM.


Reminder
========

It is NOT recommended to use that in production.
See the package description for the list of reasons.


Install
=======

Because rust-coreutils can easily break a system, it isn't replacing the
GNU coreutils.
Instead, we are installing the binaries in /usr/lib/cargo/bin/coreutils/.

To use rust-coreutils instead GNU, use:

$ export PATH=/usr/lib/cargo/bin/coreutils/:$PATH

As rust-coreutils aims at being a drop-in replacement, it is possible
to override the GNU coreutils binaries.

We are NOT installing the binaries in /usr/lib/cargo/bin/ to avoid unexpected errors
in case users already added /usr/lib/cargo/bin/ in their PATH to get fd or others binaries.


Repack
======

cd $TARGET
rm -rf build/coreutils
./update.sh coreutils # will fail

cd <tagged-source-of-coreutils>
cp -R $TARGET/src/coreutils/debian $TARGET/build/coreutils/
cp Cargo.toml $TARGET/build/coreutils/
cp -R src/* $TARGET/build/coreutils/src/
cd $TARGET/build
rm -rf coreutils.orig coreutils/.pc/
tar zcvf rust-coreutils_0.0.7.orig.tar.gz coreutils/
dpkg-source -b -i coreutils

For now, we are following upstream version.
If one day, we want to replace GNU/coreutils package with this one,
we will have to follow the version of the GNU version as we have some packages
having an explicit minimal dep (ex: initramfs-tools-core, xinit, initscripts)

 -- Sylvestre Ledru <sylvestre@debian.org>, Sat,  3 Apr 2021 14:04:40 +0200
